package ide.model;

import ide.gui.CanaleDiStampa;

public interface IDEModel {

    void hitCarica();
    void hitEsegui();
    void hitRinomina();
    void hitSalva();
    
    void setCode(String code);
    void setCanaleDiStampa(CanaleDiStampa c);
    
}
