package ide.model;

import linguaggio.interprete.Interprete;
import linguaggio.interprete.InterpreteCheDebboDireMiSoddisfa;
import ide.gui.CanaleDiStampa;

public class SuperMegaModelIDE implements IDEModel {

    private final Interprete exe;
    private String codice = "";
    public SuperMegaModelIDE(CanaleDiStampa cs){
	exe = new InterpreteCheDebboDireMiSoddisfa(cs);
    }
    
    @Override
    public void hitCarica() {
	// TODO Auto-generated method stub

    }

    @Override
    public void hitEsegui() {
	exe.eseguiProgramma(codice);
    }

    @Override
    public void hitRinomina() {
	// TODO Auto-generated method stub

    }

    @Override
    public void hitSalva() {
	// TODO Auto-generated method stub

    }

    @Override
    public void setCode(String code) {
	codice = code;
    }

    @Override
    public void setCanaleDiStampa(CanaleDiStampa c) {
	// TODO Auto-generated method stub

    }

}
