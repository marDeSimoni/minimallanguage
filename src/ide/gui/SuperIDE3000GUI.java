package ide.gui;

import ide.model.IDEModel;
import ide.model.SuperMegaModelIDE;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import linguaggio.interprete.Grammatica;

public class SuperIDE3000GUI implements CanaleDiStampa {

    private final JFrame gui = new JFrame();
    private final String TITOLO = "Super IDE 3000";
    private final BufferedImage logo = initImg("ide/minimal.png");
    private final JTextArea log = new JTextArea(10, 10);
    private final IDEModel model = new SuperMegaModelIDE(this);

    private BufferedImage initImg(String fileName) {
	BufferedImage img = null;

	try {
	    img = ImageIO.read(this.getClass().getClassLoader()
		    .getResourceAsStream(fileName));
	} catch (IOException e) {
	    e.printStackTrace();
	}

	return img;
    }

    public SuperIDE3000GUI() {
	gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	gui.setIconImage(logo);

	final JPanel main = new JPanel();
	main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
	JLabel titolo = new JLabel(TITOLO);
	main.add(titolo);

	final JPanel lower = new JPanel();
	final JPanel left = new JPanel();
	final JPanel right = new JPanel();
	lower.setLayout(new GridLayout(1, 2));

	left.setLayout(new BoxLayout(left, BoxLayout.Y_AXIS));
	
	right.setLayout(new GridLayout(2,1));
	
	JPanel buttons = new JPanel();

	final JLabel titolo_codice = new JLabel("Il tuo codice:");
	final JTextArea codice = new JTextArea(15, 15);
	String esempioDiCodice = "��esempio di codice��\nnumero primo = 22\nnumero diviso = 0\nnumero i = 2\nnumero ultimoDivisore=1\n"+""
		+ "finch� i*ultimoDivisore<primo | i*ultimoDivisore=primo\n"
+"se primo%i=0\ndiviso = 1\nstampa i\naltrimenti\n��non fare nulla��\nfine\ni=i+1\nfine\nse diviso = 1\n"
+"stampa il numero non � primO e I suoi divisori sono stampati sopra\naltrimenti\nstampa il numero � primO\n"
+"fine";
	codice.setText(esempioDiCodice);
	final JLabel titolo_log = new JLabel("Risultato del codice:");
	log.setDisabledTextColor(Color.GRAY);
	log.setEnabled(false);

	left.add(log, 0);
	left.add(titolo_log, 0);
	left.add(codice, 0);
	left.add(titolo_codice, 0);

	// un pulsante per eseguire, uno per caricare, uno per salvare e uno per
	// rinominare il path su cui si scriver�.
	JButton esegui = new JButton("Esegui codice");
	esegui.addActionListener(e -> {

	    log.setText("");
	    model.setCode(codice.getText());
	    model.hitEsegui();

	});
	buttons.add(esegui);

	/*
	 TODO capire se questi pulsanti possono servire
	JButton carica = new JButton("Carica codice");
	JButton salva = new JButton("Salva codice");
	JButton rinomina = new JButton("Rinomina codice");
	*/
	
	String tips = "Words comandi e consigli:\n\n"
	+"istanze:\t"+Grammatica.istanza_numero+","+Grammatica.istanza_carattere+","+Grammatica.istanza_stringa+"\n"
	+"if:\t"+Grammatica.se+"\n"
	+"else:\n"+Grammatica.altrimenti+"\n"
	+"while:\t"+Grammatica.finch�+"\n"
	+"fine if e while:\n"+Grammatica.fineIpotesi+"\n"
	+"operatori aritmetici:\t"+"+,-,*,/,%"+"\n"
	+"simboli di relazione d'ordine:\t"+"<,>,="+"\n"
	+"operatori logici:\t"+ "|,&"+"\n"
	+"inizio commento:"+ Grammatica.inizioCommentoRigheMultiple+"\n"
	+"fine commento:"+ Grammatica.fineCommentoRigheMultiple+"\n"+"\n"
	+"comando di stampa:\t"+Grammatica.stampa+"\n\n"
	+"Il programma non parte.. hai chiuso tutti gli if e while con la word di fine?\n";
	
	JTextArea tipsArea = new JTextArea(tips);
	tipsArea.setEditable(false);
	tipsArea.setBackground(Color.GRAY);
	
	right.add(buttons,0);
	right.add(tipsArea, 1);

	lower.add(left, 0);
	lower.add(right, 1);
	main.add(lower);

	gui.getContentPane().add(main);
	gui.pack();
	gui.setVisible(true);
    }

    public static void main(String[] args) {

	new SuperIDE3000GUI();
    }

    @Override
    public void stampa(String s) {

	log.append(s+'\n');
    }
    
    

}