package linguaggio.variabili;

public class AstrattaVariabile<X> implements Variabile<X> {


    private final String nome;
    private X valore;
    
    AstrattaVariabile( String nome ){
	
	this.nome = nome;
	
    }
    @Override
    public String getName() {
	
	return nome;
    }

    @Override
    public X getValue() {
	
	return valore;
    }

    @Override
    public void setValue(X nuovoValore) {

	this.valore = nuovoValore;
	
    }
    
    
}
