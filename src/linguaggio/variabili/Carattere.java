package linguaggio.variabili;

public class Carattere extends AstrattaVariabile<Character> {

    public Carattere(String nome ) {
	super(nome);
    }

    @Override
    public String toString(){
	
	return Character.toString(this.getValue());
    }
    
}
