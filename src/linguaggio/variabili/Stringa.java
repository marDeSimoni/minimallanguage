package linguaggio.variabili;

public class Stringa extends AstrattaVariabile<String>{

    public Stringa(String nome) {
	super(nome);
    }

    @Override
    public String toString(){
	
	return getValue();
    }
}
