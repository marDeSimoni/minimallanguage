package linguaggio.variabili;

public class Numero extends AstrattaVariabile<Double>{

    public Numero(String nome) {
	super(nome);
    }

    @Override
    public String toString(){
	
	return Double.toString(this.getValue());
    }
    
}
