package linguaggio.variabili;

public interface Variabile<X> {

    String getName();
    X getValue();
    void setValue(X x);
}
