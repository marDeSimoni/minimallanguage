package linguaggio.interprete;

import ide.gui.CanaleDiStampa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import linguaggio.variabili.*;

/*
 * 
 * DIFESE DAI SIDE EFFECTS OVUNQUE
 * 
 */

public class InterpreteCheDebboDireMiSoddisfa implements Interprete {

    @SuppressWarnings("rawtypes")
    private final Map<String, Variabile> variabili = new HashMap<>();
    private final Calcolatore c = new CalcolatoreDavveroMoltoTosto();
    private final CanaleDiStampa canale;

    /**
     * Mi immagino un assegnamento del tipo etichettaVar = valore
     */
    public InterpreteCheDebboDireMiSoddisfa(final CanaleDiStampa c) {

	canale = c;

    }

    private List<String> difendi(final List<String> lista) {

	return new ArrayList<String>(lista);

    }

    private List<String> sostituisciValoriNellEspressione(
	    final List<String> espressione) {

	List<String> nuovaEspressione = difendi(espressione);

	for (int i = 0; i < nuovaEspressione.size(); i++) {
	    if (variabili.containsKey(nuovaEspressione.get(i))) {
		nuovaEspressione.add(i, variabili.get(nuovaEspressione.get(i)).getValue()
			.toString());
		nuovaEspressione.remove(i + 1);
	    }
	}
	return nuovaEspressione;

    }

    @SuppressWarnings("unchecked")
    // Ci vuole fiducia: supress unchecked
    private void eseguiAssegnamento(final List<String> assegnamento) {
	if (variabili.get(assegnamento.get(0)) instanceof Numero) {
	    // assegnamento del tipo num = espressione complicata
	    List<String> espressione = difendi(assegnamento);
	    espressione.remove(0); // rimuovo il nome dell'etichetta
	    espressione.remove(0); // rimuovo l'uguale

	    variabili.get(assegnamento.get(0)).setValue(
		    c.calcola_espressione_matematica(this
			    .sostituisciValoriNellEspressione(espressione)));
	}

	else {
	    variabili.get(assegnamento.get(0)).setValue(assegnamento.get(2));
	}
    }

    /**
     * Mi immagino una forma di istanza tipo
     * carattere c = f | carattere c oppure
     * stringa a = ciao | stringa a oppure
     * numero n = espressione | numero = n
     * 
     * @param istanza
     *            L'istruzione di istanza
     */
    private void eseguiIstanza(List<String> istanza) {

	String etichettaVar = istanza.get(1);
	if (!variabili.containsKey(etichettaVar)) {
	    if (istanza.get(0).equals(Grammatica.istanza_carattere)) {
		variabili.put(etichettaVar, new Carattere(etichettaVar));
	    } else if (istanza.get(0).equals(Grammatica.istanza_numero)) {

		variabili.put(etichettaVar, new Numero(etichettaVar));
	    } else if (istanza.get(0).equals(Grammatica.istanza_stringa)) {

		variabili.put(etichettaVar, new Stringa(etichettaVar));

	    }
	}
	if (istanza.size() > 2) {
	    List<String> assegnamento = this.difendi(istanza);
	    assegnamento.remove(0);
	    eseguiAssegnamento(assegnamento);
	}

    }
/*
 * numero primo = 23
numero diviso = 0
numero i = 0
finch� i<primo
se primo%i>0
diviso = 1
altrimenti
stampa primo
fine
i=i+1
fine
stampa diviso
 */
    private void eseguiSe(List<String> condizione,
	    List<List<String>> istruzioni, List<List<String>> altrimenti) {

	
	if (Boolean
		.parseBoolean(c
			.calcola_espressione_matematica(sostituisciValoriNellEspressione(condizione)))) {

	    eseguiSequenzaDiIstruzioni(istruzioni);
	} else if(altrimenti!=null){

	    eseguiSequenzaDiIstruzioni(altrimenti);

	}

    }

    private void eseguiWhile(List<String> condizione,
	    List<List<String>> istruzioni) {
	
	while (Boolean
		.parseBoolean(c
			.calcola_espressione_matematica(sostituisciValoriNellEspressione(condizione)))) {

	    eseguiSequenzaDiIstruzioni(istruzioni);

	}

    }

    /**
     * Esegue istruzioni da una riga: assegnamenti o istanze..
     * Ma non: for, while..
     * 
     * @param istruzione
     * @return
     */
    private void eseguiSingolaIstruzione(List<String> istruzione) {
	if (Grammatica.isIstanza(istruzione.get(0))) {

	    eseguiIstanza(istruzione);

	} else if (variabili.containsKey(istruzione.get(0))
		&& istruzione.get(1).charAt(0) == Grammatica.segnoAssegnamento) {

	    eseguiAssegnamento(istruzione);

	}
    }

    private void eseguiStampa(List<String> daStampare) {

	List<String> s = sostituisciValoriNellEspressione(daStampare);
	
	StringBuilder sb = new StringBuilder(40);
	for (String x : s) {
	    sb.append(x + " ");
	}
	canale.stampa(sb.toString());

    }

    private void eseguiSequenzaDiIstruzioni(List<List<String>> istruzioni) {

	for (int i = 0; i < istruzioni.size(); i++) {

	    List<String> istruzione = istruzioni.get(i);

	    if (Grammatica.isIpothesysWord(istruzione.get(0))) {

		String tipoIpotesi = istruzione.get(0);

		List<String> condizioneDellIpotesi = difendi(istruzione);
		condizioneDellIpotesi.remove(0);
		List<List<String>> istruzioniDellIpotesi = new ArrayList<>();
		List<List<String>> istruzioniDellAltrimenti = new ArrayList<>();
		boolean altrimenti = false;
		int ulterioriIpotesiAperte = 0;
		i++;

		while (!istruzioni.get(i).get(0).equals(Grammatica.fineIpotesi)
			|| ulterioriIpotesiAperte != 0) {
		    
		    if (Grammatica.isIpothesysWord(istruzioni.get(i).get(0))) {
			ulterioriIpotesiAperte++;
		    } else if (istruzioni.get(i).get(0)
			    .equals(Grammatica.fineIpotesi)) {
			ulterioriIpotesiAperte--;
		    } else if (istruzioni.get(i).get(0)
			    .equals(Grammatica.altrimenti)) {
			altrimenti = true;
		    }
		    if (altrimenti && tipoIpotesi.equals(Grammatica.se) ) {
			istruzioniDellAltrimenti
				.add(difendi(istruzioni.get(i)));
		    } else {
			istruzioniDellIpotesi.add(difendi(istruzioni.get(i)));
		    }
		    i++;
		}
		// istruzione.get(i).equals(Grammatica.fineIpotesi)==true
		if (tipoIpotesi.equals(Grammatica.finch�))
		    eseguiWhile(condizioneDellIpotesi, istruzioniDellIpotesi);
		else if (tipoIpotesi.equals(Grammatica.se)) {
		    eseguiSe(condizioneDellIpotesi, istruzioniDellIpotesi,
			    istruzioniDellAltrimenti);
		} else if (tipoIpotesi.equals(Grammatica.per)) {

		}
	    } else {
		if (istruzioni.get(i).get(0).equals(Grammatica.stampa)) {

		    List<String> daStampare = difendi(istruzioni.get(i));
		    daStampare.remove(0);
		    eseguiStampa(daStampare);

		} else {
		    eseguiSingolaIstruzione(istruzione);
		}
	    }
	}

    }

    public void eseguiProgramma(String programma) {

	Scanner sc = new ScannerNotSoBad();
	List<String> s = sc.scannerizza(programma);
	Parser p = new PoorPoorParser();
	/*
	 * Mi aspetto istanze, assegnamenti o
	 * se condizione
	 * istruzioni
	 * fine ipotesi
	 * oppure
	 * finch� condizione
	 * istruzione
	 * fine ipotesi
	 */
	List<List<String>> istruzioni = p.parse(s);
	
	eseguiSequenzaDiIstruzioni(istruzioni);

    }
    // TODO decidere se il for deve essere incluso o no. Io
    // credo di no.
    // TODO le funzioniiii

    @Override
    public void ricomincia() {
	this.variabili.clear();
    }

}
