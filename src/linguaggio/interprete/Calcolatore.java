package linguaggio.interprete;

import java.util.List;

public interface Calcolatore {

    String calcola_espressione_matematica(List<String> espressione);
    
}
