package linguaggio.interprete;

import java.util.List;

public interface Scanner {

	public List<String> scannerizza(String testo);
	
}
