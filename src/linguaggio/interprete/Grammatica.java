package linguaggio.interprete;

/* 	Regole:
 * 
 * 	blank per le istruzioni -> \n
 * 
 * 	Il primo carattere di una stringa � un carattere.
 * 
 * 	numero -> double parsabile dal comando java Double.parseDouble(String s)
 * 	operatore -> uno fra + - * / % ^ con la precedenza standard
 * 	stringa stringa -> istanza
 * 	istanza_xxx stringa = valore_di_tipo_xxx -> istanza e assegnamento 
 * 
 * 	con ipotesi = se mentre
 *
 *	espressione matematica -> 	valore numerico operatore valore numerico  
 *	valore numerico ->	numero | apertaParentesi espressione matematica chiusaParentesi
 *
 * 	ipotesi espressione booleana -> inizio ipotesi
 * 	fine ipotesi -> fine dell'if
 * 
 */

public final class Grammatica {

    public static final char[][] precedenzaOperazionale = { { '^' },
	    { '*', '/', '%' }, { '-', '+' }, { '<', '>' }, { '=' },{'&','|'} };

    public static final char[] immondizia = { ' ' };
    public static final char segnoAssegnamento = '=';
    
    public static final String commentoRiga = "#";
    
    public static final String inizioCommentoRigheMultiple = "��";
    public static final String fineCommentoRigheMultiple = "��";
    //Words
    public static final String istanza_numero = "numero";
    public static final String istanza_carattere = "carattere";
    public static final String istanza_stringa = "stringa";
    public static final String per = "per";
    public static final String se = "se";
    public static final String finch� = "finch�";
    public static final String altrimenti = "altrimenti";
    public static final String fineIpotesi = "fine";

    //Comandone
    public static final String stampa = "stampa";
    
    public static final String[] words = { istanza_numero, istanza_carattere,
	    istanza_stringa, per, se, finch�, fineIpotesi,stampa };

    public static boolean isIstanza(String s) {

	return s.equals(istanza_numero) || s.equals(istanza_carattere)
		|| s.equals(istanza_stringa);

    }
    public static boolean isIpothesysWord(String s){
	return s.equals(finch�) || s.equals(per) || s.equals(se);
    }
    
    public static boolean isBlank(char c) {

	return c == '\n';

    }

    public static boolean isOperator(char a) {

	boolean operatore = false;
	for (char[] sottoArrayCheSeFosseUnoUnicoSarebbeMeglio : Grammatica.precedenzaOperazionale)
	    if (!operatore) {
		operatore = Grammatica.isInArray(a,
			sottoArrayCheSeFosseUnoUnicoSarebbeMeglio);
	    }
	return operatore;
    }

    public static boolean isParenthesys(char c) {

	return c == '(' || c == ')' || c == '{' || c == '}' || c == '['
		|| c == ']';

    }

    public static boolean isParsingSeparator(char c) {

	return isOperator(c) || c == ';' || isParenthesys(c) || isBlank(c)|| c==' ' || c=='\n';

    }

    public static boolean isMathOperator(char c) {

	return c == '+' || c == '-' || c == '*' || c == '/' || c == '%'
		|| isMathOperatorReturningBoolean(c);
    }

    public static boolean isMathOperatorReturningNumber(char c) {

	return !isMathOperatorReturningBoolean(c);

    }

    public static boolean isMathOperatorReturningBoolean(char c) {

	return c == '<' || c == '>' || c == '=';

    }

    public static boolean isLogicalOperator(char c) {

	return isOperator(c) && !isMathOperator(c);

    }

    public static boolean isInArray(char c, char[] cs) {

	boolean trovato = false;
	for (char c_in_array : cs)
	    if (c_in_array == c)
		trovato = true;
	return trovato;

    }

}
