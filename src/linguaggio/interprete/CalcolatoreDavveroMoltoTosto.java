package linguaggio.interprete;

import java.util.ArrayList;
import java.util.List;

public class CalcolatoreDavveroMoltoTosto implements Calcolatore {

    public String calcola_espressione_matematica(List<String> espressione) {

	// Risolvi tutte le espressioni nelle parentesi
	for (int i = 0; i < espressione.size(); i++) {

	    if (espressione.get(i).equals("(")) {
		int stringheUtilizzate = 1;
		int parentesiAperte = 1;
		List<String> sottoespressione = new ArrayList<>();
		// Parsing dell'espressione con parentesi

		while (parentesiAperte != 0) {
		    if (espressione.get(i + stringheUtilizzate).equals(")")) {
			parentesiAperte--;
		    } else if (espressione.get(i + stringheUtilizzate).equals(
			    "(")) {
			parentesiAperte++;
		    }
		    sottoespressione.add(espressione
			    .get(i + stringheUtilizzate));
		    stringheUtilizzate++;
		}
		sottoespressione.remove(stringheUtilizzate - 2);

		String risultato = calcola_espressione_matematica(sottoespressione);

		while (stringheUtilizzate > 0) {
		    espressione.remove(i);
		    stringheUtilizzate--;
		}

		espressione.add(i, risultato);
	    }
	}
	// Poi calcola
	return calcola_espressione_matematica_senza_parentesi(espressione);
    }

    private String calcola_espressione_matematica_senza_parentesi(
	    List<String> espressione) {

	char operatore;
	String primo, secondo;
	for (int i = 0; i < Grammatica.precedenzaOperazionale.length; i++) {
	    for (int k = 0; k < espressione.size(); k++) {

		if (Grammatica.isInArray(espressione.get(k).charAt(0),
			Grammatica.precedenzaOperazionale[i])) {

		    operatore = espressione.get(k).charAt(0);
		    primo = espressione.get(k - 1);
		    secondo = espressione.get(k + 1);
		    String risultato = this.calcola_operazione_duale(primo,
			    secondo, operatore);

		    // Sostituzione del risultato
		    espressione.remove(k - 1);
		    espressione.remove(k - 1);
		    espressione.remove(k - 1);
		    espressione.add(k - 1, risultato);
		    k -= 2;
		}
	    }

	}

	return espressione.get(0);

    }

    private String calcola_operazione_duale(String primo_generico,
	    String secondo_generico, char operatore) {

	if (Grammatica.isMathOperator(operatore)) {

	    double primo = Double.parseDouble(primo_generico), secondo = Double
		    .parseDouble(secondo_generico);
	    if (Grammatica.isMathOperatorReturningNumber(operatore)) {
		double risultato = 0;
		switch (operatore) {
		case '+':
		    risultato = primo + secondo;
		    break;

		case '-':
		    risultato = primo - secondo;
		    break;
		case '*':
		    risultato = primo * secondo;
		    break;
		case '/':
		    risultato = primo / secondo;
		    break;
		case '^':
		    risultato = Math.pow(primo, secondo);
		    break;
		case '%':
		    risultato = ((int) primo) % ((int) secondo);
		    break;
		}

		return new Double(risultato).toString();
	    } else if (Grammatica.isMathOperatorReturningBoolean(operatore)) {
		boolean risultato = false;
		switch (operatore) {
		case '<':
		    risultato = primo < secondo;
		    break;

		case '>':
		    risultato = primo > secondo;
		    break;
		case '=':
		    risultato = primo == secondo;
		    break;

		}

		return new Boolean(risultato).toString();

	    }
	} else if (Grammatica.isLogicalOperator(operatore)) {

	    boolean primo = Boolean.parseBoolean(primo_generico), secondo = Boolean
		    .parseBoolean(secondo_generico);
	    boolean risultato = false;
	    switch (operatore) {
	    case '&':
		risultato = primo && secondo;
		break;
	    case '|':
		risultato = primo || secondo;
		break;

	    }
	    return new Boolean(risultato).toString();
	}
	return null; // qualcosa � andato storto -> null
    }

}
