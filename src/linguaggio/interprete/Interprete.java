package linguaggio.interprete;

public interface Interprete {

    void eseguiProgramma(String programma);
    void ricomincia();
}
