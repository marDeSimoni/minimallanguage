package linguaggio.interprete;

import java.util.List;

public interface Parser {

	public boolean giudicaCorrettezzaGrammatica(String programma);

	public List<List<String>> parse(List<String> s);

}
