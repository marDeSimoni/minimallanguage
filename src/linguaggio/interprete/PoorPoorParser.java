package linguaggio.interprete;

import java.util.ArrayList;
import java.util.List;

public class PoorPoorParser implements Parser{

    @Deprecated
    public boolean giudicaCorrettezzaGrammatica(String programma) {

	return true; // LOL

    }

    public List<List<String>> parse(List<String> s) {

	List<List<String>> istruzioni = new ArrayList<>();
	istruzioni.add(new ArrayList<>());

	int i_lista_di_istruzioni = 0;
	int i_lista_parole = 0;

	/*
	 * finch� non ho visto tutte le parole;
	 *  se una certa parola in sequenza � il blank.
	 *  e se ho messo gi� qualcosa nell'ultima istruzione.
	 *       creo una nuova istruzione e aggiorno l'iteratore.
	 *    altrimenti resto nella vecchia istruzione(vuota).
	 *  altrimenti ho una parola.
	 *  e la metto nell'istruzione.
	 */

	while (i_lista_parole < s.size() ) {
	    if (Grammatica.isBlank(s.get(i_lista_parole).charAt(0))) { // Ho
								       // trovato
								       // il
								       // blank
		if (istruzioni.get(i_lista_di_istruzioni).size() != 0) {
		    // Se ho messo qualcosa, vado avanti
		    istruzioni.add(new ArrayList<>());
		    i_lista_di_istruzioni++;

		}
	    } else { // E' una parola: appendo la parola
		istruzioni.get(i_lista_di_istruzioni)
			.add(s.get(i_lista_parole));
	    }
	    i_lista_parole++; // A prescindere, vado oltre

	}

	return istruzioni;
    }

   
}
