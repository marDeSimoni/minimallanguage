package linguaggio.interprete;

import java.util.ArrayList;
import java.util.List;

public class ScannerNotSoBad implements Scanner {

	public List<String> scannerizza(String testo) {

		List<String> parole = new ArrayList<>();
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < testo.length(); i++) {

			if (Grammatica.isParsingSeparator(testo.charAt(i))) {
				parole.add(builder.toString());
				parole.add(testo.charAt(i) + "");
				builder = new StringBuilder();
				
			} else if (testo.substring(i, i + Grammatica.commentoRiga.length())
					.equals(Grammatica.commentoRiga)) {
				//TODO Debuggare questo blocco
				while (!Grammatica.isBlank(testo.charAt(i))) {
					i++;
				}
				i++;
				
			} else if (testo.length()-Grammatica.fineCommentoRigheMultiple.length()-i>=0 && testo.substring(i,
					i + Grammatica.inizioCommentoRigheMultiple.length())
					.equals(Grammatica.inizioCommentoRigheMultiple)) {
				i+=Grammatica.inizioCommentoRigheMultiple.length();
				while (!testo.substring(i,
						i + Grammatica.fineCommentoRigheMultiple.length())
						.equals(Grammatica.fineCommentoRigheMultiple)) {
					i++;
				}
				i--;
				i += Grammatica.fineCommentoRigheMultiple.length();
			} else if (!Grammatica.isInArray(testo.charAt(i),
					Grammatica.immondizia)) { // elimino
				// i caratteri di garbage
				builder.append(testo.charAt(i));
			}
		}

		parole.add(builder.toString());
		List<String> b = new ArrayList<>();
		for (String s : parole )
		    if(s.replaceAll(" ", "").length()>0)
			b.add(s);
		return b;

	}

}
